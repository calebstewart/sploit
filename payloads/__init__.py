from pwn import *
import threading
import time
import netifaces

# Defines a generic payload. Most payloads will fall under either the "Reverse" or "Bind"
# subclasses below, but this is also available
class BasePayload(object):

    arch = 'none'
    target = 'none'

    # Static function to add all arguments expected by this payload
    @classmethod
    def add_arguments(cls, parser):
        """ Add arguments which this payload expects """
        pass

    def __init__(self, config, encoder):
        super(BasePayload, self).__init__()
        self.config = config
        self.encoder = encoder
        self.artifacts = []
        self.routines = []
        self.connection = None
        pass

    def get_connection(self):
        log.error('payload: no connection created')

    # Generate the payload which the Exploit will send to the target
    def generate(self):
        log.info('payload: no generate function implemented')

    # Called before sending the payload. Will setup separate threads, listeners, etc
    def setup(self):
        pass

    # Called after the exploit completes. Used to interact with the payload.
    # This should set the self.connection object so we can have automated
    # interactions later
    def interact(self):
        pass

    # Cleanup any artifacts left by either the payload or added by the exploit
    def cleanup(self):
        log.info('cleaning up {0} artifacts'.format(len(self.artifacts)))
        for artifact in self.artifacts:
            self.connection.rm(artifact)
        log.info('executing cleanup routines')
        for routine in self.routines:
            routine[0](*routine[1], **routine[2])
        if 'init' in self.config:
            log.info('executing init script')
            self.connection.send_raw(self.config['init'])
        pass

    def add_routine(self, routine, *args, **kwargs):
        self.routines.append((routine, args, kwargs))

    # do all serialization, including payload encoding using the specified encoder
    def serialize(self):
        return self.encoder.encode(self.generate())

    # Add an artifact to the stack to be cleaned up
    def add_artifact(self, artifact):
        self.artifacts.append(artifact)

    # Generate payload via conversion to string
    def __str__(self):
        return self.serialize()

# This payload should start a process which attempts to connect back to the host
# upon running on config['lhost']:config['lport'].
class ReversePayload(BasePayload):

    options = [
        { 'name': 'interface', 'help': 'Interface name to find local host address', 'required': False },
        { 'name': 'lhost', 'help': 'Local Host (attacker) address', 'required': False },
        { 'name': 'lport', 'default': random.randint(1000, 65535), 'help': 'Local Port', 'required': True }
    ]

    def __init__(self, config, encoder):
        super(ReversePayload, self).__init__(config, encoder)

    # This will start the thread, and in turn start our listener
    def setup(self):
        # find the interface IP address to listen on
        if self.config['interface'] != None:
            self.config['lhost'] = netifaces.ifaddresses(self.config['interface'])[netifaces.AF_INET][0]['addr']
        elif self.config['lhost'] == None:
            log.error('no interface or lhost specified')
    
        self.socket = listen(self.config['lport'], bindaddr=self.config['lhost'])

    # When the main thread tries to interact, wait for listener to finish
    def interact(self):
        self.socket.wait_for_connection()
        # automated actions don't seem to work right away. Probably still setting up the
        # connection on the other end. We induce a litte artifical wait time to ensure
        # everything is good to go by the time we send a command.
        time.sleep(0.5)

    @classmethod
    def add_arguments(cls,parser):
        parser.add_argument('--interface', '-i', default=None, help='Interface name to find local host address')
        parser.add_argument('--lhost', default=None, help='Local Host (attacker) address')
        parser.add_argument('--lport', default=random.randint(1000, 65535), help='Local Port')


# This is a simple payload which will open a listener on the target machine 
# and will then connect to the new listener after the exploit runs.
class BindPayload(BasePayload):

    options = [
        { 'name': 'bport', 'default': random.randint(1000, 65535), 'help': 'The port to bind to on the remote host', 'required': True}
    ]

    def __init__(self, config, encoder):
        super(BasePayload, self).__init__(config, encoder)

    # Bind is simple. Just connect to the remote host, and call our connected
    # function
    def interact(self):
        self.socket = remote((self.config['rhost'], self.config['lport']))

















    