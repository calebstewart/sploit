import imp
import uuid

# Load a module based on its full name (e.g. "package.module.submodule")
def load_module(name):
    module = None
    prev_path = None
    for m in name.split('.'):
        if module != None:
            prev_path = module.__path__
        (mfile, mpath, minfo) = imp.find_module(m, prev_path)
        module = imp.load_module(name, mfile, mpath, minfo)
        #prev_path = module.__path__
    return module