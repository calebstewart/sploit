from pwn import *

class Architecture(object):

    name=None
    pathsep='/'
    
    @classmethod
    def get_temp_dir(cls):
        log.error('unknown temp directory for {0}'.format(cls.__name__))

    @classmethod
    def get_temp_file(cls, ext=''):
        return '{0}{1}{2}.{3}'.format(cls.get_temp_dir(),cls.pathsep,str(uuid.uuid4()),ext)

class Windows(Architecture):
    name = 'windows'
    pathsep = '\\'

    @classmethod
    def compatible(cls, other):
        return other.name.startswith('windows')

    @classmethod
    def get_temp_dir(cls):
        return 'C:\\Windows\\Temp'

class Windows32(Architecture):
    name = 'windows32'
    pathsep = '\\'

    @classmethod
    def compatible(cls, other):
        return cls == other

    @classmethod
    def get_temp_dir(cls):
        return 'C:\\Windows\\Temp'

class Windows64(Windows32):
    name = 'windows64'

class Unix(Architecture):
    name = 'unix'

    @classmethod
    def compatible(cls, other):
        return other.name.startswith('unix')

    @classmethod
    def get_temp_dir(cls):
        return '/tmp'

class Unix32(Architecture):
    name = 'unix32'

    @classmethod
    def compatible(cls, other):
        return cls == other

    @classmethod
    def get_temp_dir(cls):
        return '/tmp'

class Unix64(Unix32):
    name = 'unix64'