from encoders import BaseEncoder
import base64

class Encoder(BaseEncoder):
    def __init__(self, config):
        super(Encoder, self).__init__(config)

    def encode(self, data):
        return base64.b64encode(data)

    def decode(self, data):
        return base64.b64decode(data)