from pwn import *
from encoders import BaseEncoder

class Encoder(BaseEncoder):

    options = [
        { 'name': 'key', 'help': 'the xor encoding key', 'default': }
    ]

    def __init__(self, config):
        super(Encoder, self).__init__(config)
        if 'key' not in config:
            log.error('{0}.{1}: no key specified'.format(self.__module__, Encoder.__qualname__))
        else:
            self.key = u32(config['key'])

    def encode(self, data):
        chunks = []
        key = self.key #int(self.config['key'], 16)
        for chunk in [data[i:i+4] for i in range(0, len(data), 4)]:
            chunks.append(u32(chunk) ^ key)

        code = 'jmp decode\n'
        code += 'data:\n'
        for chunk in chunks:
            code += '.4byte {0}\n'.format(hex(chunk))
        code += 'decode:\n'
        code += 'pop esi\n'
        code += 'add esi,2\n'
        code += shellcraft.i386.xor(key, 'esi', len(''.join(p32(chunk))))
        code += 'jmp data\n'

        return asm(code)
        