from pwn import *
from encoders import BaseEncoder
import base64

class Encoder(BaseEncoder):

    options = []

    def __init__(self, config):
        super(Encoder, self).__init__(config)

    def encode(self, data):
        return 'eval(base64_decode("{0}"));'.format(base64.b64encode(data))
