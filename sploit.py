#!/usr/bin/env python2
from pwn import *
import sys
import argparse
import json
import util
import netifaces
import random

# Make sure we find the local packages (first current directory)
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, os.getcwd())

# The first argparser is for getting the config file
conf_parser = argparse.ArgumentParser(
    description='Flexible exploit framework allowing custom exploits as well as pre-canned implementations',
    add_help=False)
conf_parser.add_argument('--config', '-c',
    default='config.json',
    help='Exploit configuration file')
args, remaining = conf_parser.parse_known_args()

# Open and read configuration information
config = {}
with open(args.config, 'r') as f:
    config = json.loads(f.read())

# The second parser is for the modules
mod_parser = argparse.ArgumentParser(
    parents=[conf_parser],
    add_help=False
)
mod_parser.set_defaults(**config)
mod_parser.add_argument('--exploit', '-e', help='Exploit selection')
mod_parser.add_argument('--payload', '-p', help='Payload selection')
mod_parser.add_argument('--encoder', '-enc', default='encoders.none', help='Encoder selection')
args, remaining = mod_parser.parse_known_args()
args = vars(args)

# Load modules
exploit_module = util.load_module(args['exploit'])
payload_module = util.load_module(args['payload'])
encoder_module = util.load_module(args['encoder'])

# The last parser is for module specific parameters
parser = argparse.ArgumentParser(description=exploit_module.Exploit.__doc__, parents=[mod_parser])
# Proxy is valid for basically everything. We just add it here.
parser.add_argument('--proxy', default=None, help='Proxy host:port')
# We assume all exploits are remote exploits
parser.add_argument('--rhost', default=None, help='Remote Host (target) address')
parser.add_argument('--rport', default=None, type=int, help='Remote Port')

# Add module specific arguments
options = exploit_module.Exploit.options + \
            payload_module.Payload.options + \
            encoder_module.Encoder.options
for option in options:
    if 'type' not in option:
        option['type'] = None
    if 'default' in option:
        parser.add_argument('--'+option['name'], default=option['default'], help=option['help'], type=option['type'])
    else:
        parser.add_argument('--'+option['name'], help=option['help'], type=option['type'])

# Set the defaults from the config so far
parser.set_defaults(**config)
# Parse arguments
config = vars(parser.parse_args())

for option in options:
    if (option['required'] and option['name'] not in config) or \
        (option['required'] and config[option['name']] == None):
        log.error('missing required option: {0}'.format(option['name']))

exploit = None
payload = None
encoder = None
    
# Dump some useful info instead of running the exploit
#if config['info']:
#    log.info('{payload} via {exploit} against {rhost}:{rport}, encoded with {encoder}'.format(
#        payload=config['payload'], exploit=config['exploit'], rhost=config['rhost'],
#        encoder=config['encoder'], rport=config['rport']
#    ))
#    sys.exit(0)

# Make sure the arch of the payload matches the exploit
if payload_module.Payload.target not in exploit_module.Exploit.targets:
    log.error('{0} is not compatible with {1}. Compatible targets: {2}'.format(
        config['payload'], config['exploit'], str(exploit_module.Exploit.targets)
    ))

# Create payload, encoder, and exploit objects
encoder = encoder_module.Encoder(config)
payload = payload_module.Payload(config, encoder)
exploit = exploit_module.Exploit(config, payload)

# Do any pre-exploit setup
log.info('setting up payload')
payload.setup()

# Send the exploit
log.info('executing exploit')
exploit.start()

# Interact with the exploit, if successful
log.info('exploit started, initiating session')
payload.interact()

